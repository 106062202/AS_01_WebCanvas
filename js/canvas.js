const canvas = document.getElementById('webCanvas');
const eraserBtn = document.getElementById('eraser_btn');
const brushBtn = document.getElementById('brush_btn');
const brushSizeMenu = document.getElementById('brush_size_menu');
const submitTextBtn = document.getElementById('submit_text_btn');
const textBox = document.getElementById('text_box');
const fontMenu = document.getElementById('font_menu');
const fontSizeSlider = document.getElementById('font_size_slider');
const refreshBtn = document.getElementById('refresh_btn');
const downloadLink = document.getElementById('download_link');
const circleTool = document.getElementById('circle_tool');
const ctx = canvas.getContext('2d');
const points = [];

let textFont = 'monospace';
let fontSize = '39px ';

function getMousePos(event) {
  const rect = canvas.getBoundingClientRect();

  return {
    x: event.clientX - rect.left,
    y: event.clientY - rect.top,
  };
}

function mouseMove(event) {
  points.push(getMousePos(event));

  event.preventDefault();

  ctx.moveTo(points[0].x, points[0].y);
  ctx.beginPath();

  for (let i = 1; i < points.length; i += 1) {
    ctx.lineTo(points[i].x, points[i].y);
  }

  ctx.stroke();
}

function drawText(event) {
  const textPos = getMousePos(event);

  ctx.font = fontSize + textFont;
  ctx.fillText(textBox.value, textPos.x, textPos.y);
  textBox.value = '';

  canvas.addEventListener('mouseup', () => {
    canvas.removeEventListener('mousedown', drawText);
  });
}

function startup() {
  ctx.lineWidth = 5;
  ctx.lineJoin = 'round';
  ctx.lineCap = 'round';
  ctx.strokeStyle = '#000000';
  ctx.globalCompositeOperation = 'source-over';

  textBox.value = '';
  fontSizeSlider.value = 39;

  const colorWell = document.querySelector('#colorWell');

  colorWell.value = '#000000';

  colorWell.addEventListener('input', (event) => {
    ctx.strokeStyle = event.target.value;
  });

  colorWell.addEventListener('change', (event) => {
    ctx.strokeStyle = event.target.value;
  });

  colorWell.select();

  canvas.addEventListener('mousedown', (event) => {
    points.push(getMousePos(event));
    canvas.addEventListener('mousemove', mouseMove);
  });

  canvas.addEventListener('mouseup', () => {
    canvas.removeEventListener('mousemove', mouseMove);
    points.length = 0;
  });

  eraserBtn.addEventListener('click', () => {
    ctx.lineWidth = 20;
    ctx.strokeStyle = '#FFFFFF';
    ctx.globalCompositeOperation = 'destination-out';
  });

  brushBtn.addEventListener('click', () => {
    const idx = brushSizeMenu.selectedIndex;

    ctx.lineWidth = brushSizeMenu.options[idx].value;
    ctx.strokeStyle = colorWell.value;
    ctx.globalCompositeOperation = 'source-over';
  });

  brushSizeMenu.addEventListener('change', () => {
    if (ctx.globalCompositeOperation === 'source-over') {
      const idx = brushSizeMenu.selectedIndex;
      ctx.lineWidth = brushSizeMenu.options[idx].value;
    }
  });

  submitTextBtn.addEventListener('click', () => {
    canvas.addEventListener('click', drawText);
  });

  fontMenu.addEventListener('change', () => {
    const idx = fontMenu.selectedIndex;
    textFont = fontMenu.options[idx].value;
  });

  fontSizeSlider.addEventListener('change', () => {
    fontSize = `${fontSizeSlider.value}px `;
  });

  refreshBtn.addEventListener('click', () => {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
  });

  downloadLink.addEventListener('click', () => {
    downloadLink.href = canvas.toDataURL('image/jpg');
  });

  circleTool.addEventListener('click', () => {
    let circleCenter;

    canvas.addEventListener('mousedown', (event) => {
      canvas.removeEventListener('mousemove', mouseMove);
      circleCenter = getMousePos(event);
    });

    canvas.addEventListener('mouseup', (event) => {
      const mousePos = getMousePos(event);
      const radius = Math.hypot(mousePos.x - circleCenter.x, mousePos.y - circleCenter.y);

      ctx.fillStyle = colorWell.value;
      ctx.beginPath();
      ctx.arc(circleCenter.x, circleCenter.y, radius, 0, 2 * Math.PI);
      ctx.fill();
    });
  });
}

window.addEventListener('load', startup);
/*
document.addEventListener('click', () => {
  console.log(ctx.globalCompositeOperation);
});
*/
