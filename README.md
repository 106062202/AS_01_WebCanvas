# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

| **Item**                                         | **Score** |
| :----------------------------------------------: | :-------: |
| Basic components                                 | 60%       |
| Advance tools                                    | 35%       |
| Appearance (subjective)                          | 5%        |
| Other useful widgets (**describe on README.md**) | 1~10%     |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/04 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

---

## Put your report below here

* Brush
    - Button location: In the first row with the word _Brush_ on it.
    - How to use: After clicking on the button, hold your left mouse button and drag on the canvas.
    - Effect: Leaves lines according to the current settings on the canvas.
* Eraser
    - Button location: In the first row with the word _Eraser_ on it.
    - How to use: After clicking on the button, hold your left mouse button and drag on the canvas.
    - Effect: Wipes out the drawings on the canvas if any.
* Color Selector
    - Button location: In the second row which is colored.
    - How to use: After clicking on the colored button, click on the desired color in the color picker.
    - Effect: Changes the color of the brush to the corresponding color on the selector.
* Brush Size Menu
    - Button location: In the third row next to the name _Brush Size:_.
    - How to use: Click on the button to reveal the drop-down menu, and click on the desired option.
    - Effect: Changes the size of the brush.
* Text Box
    - Button location: In the fourth row with a box and a submit button.
    - How to use: Click on the box to enter text, then click to submit.
        Finally, click on the desired location of the canvas.
    - Effect: Puts entered text on the location clicked and clears the text box.
* Typeface Menu
    - Button location: In the fifth row next to the name _Typeface:_.
    - How to use: Click on the button to reveal the drop-down menu, and click on the desired option.
    - Effect: Changes the font of the text that will be submitted.
* Font Size Slider
    - Button location: In the sixth row next to the name _Font Size:_.
    - How to use: Click anywhere on the slider or hold and drag the button.
    - Effect: Changes the font size of the text that will be submitted.
* Reset
    - Button location: In the seventh row with the word _Reset_ on it.
    - How to use: Click on the button.
    - Effect: Clears up the canvas.
* Download Canvas
    - Button location: In the last row with the link named _Download Canvas_.
    - How to use: Click on the link.
    - Effect: Downloads the current canvas to the local computer with filename `canvas.jpg`.
